" Plugin Manager: ============================================================
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Specify a directory for plugins
" " - For Neovim: stdpath('data') . '/plugged'
" " - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

Plug 'majutsushi/tagbar'

"" Javascript Development
Plug 'pangloss/vim-javascript'
Plug 'leafgarland/typescript-vim'
Plug 'yuezk/vim-js'
Plug 'maxmellon/vim-jsx-pretty'
Plug 'prettier/vim-prettier', {
	\ 'do': 'yarn install',
	\ 'for': [
	\     'javascript', 'typescript', 'css', 'less', 
	\     'scss', 'json', 'graphql', 'markdown', 'vue', 
	\     'yaml', 'html'
	\ ]
\ }

"" Golang development
Plug 'govim/govim'
"Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'sebdah/vim-delve'
" Use release branch (recommend)
Plug 'neoclide/coc.nvim', {'branch': 'release'}

"" NerdTree
Plug 'preservim/nerdtree'
Plug 'preservim/nerdcommenter'

" Airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Plug 'itchyny/lightline.vim'

"" THEMES
Plug 'NLKNguyen/papercolor-theme'
Plug 'arcticicestudio/nord-vim'
" Plug 'rakr/vim-one', {'as': 'one'}
" Plug 'dracula/vim', { 'as': 'dracula' }
" Plug 'altercation/vim-colors-solarized'
" Plug 'joshdick/onedark.vim'

" Git
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'

" TMux
Plug 'christoomey/vim-tmux-navigator'

" Fuzzy Search
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" Erlang
Plug 'vim-erlang/vim-erlang-runtime'
Plug 'vim-erlang/vim-erlang-compiler'
Plug 'vim-erlang/vim-erlang-omnicomplete'
Plug 'vim-erlang/vim-erlang-tags'

" Elixir
Plug 'elixir-editors/vim-elixir'

" Rust
Plug 'rust-lang/rust.vim'

Plug 'dense-analysis/ale'

" V Lang
Plug 'ollykel/v-vim'

" Initialize plugin system
call plug#end()

" BASIC SETUP: ================================================================
syntax on
syntax enable
set fileencoding=utf-8
set fileformat=unix


" no legacy compatibility
set nocompatible
set nobackup
set nowritebackup
set noswapfile

" Custom
set number
set cursorline
set hlsearch
set mouse=a
set signcolumn=auto
set clipboard=unnamed

"" Should be used together
set autoindent
set smartindent
"" Search down into subfolders
"" Probides tab-completion for all file-related tasks
set path+=**
"" Display all matching files when we tab complete
set wildmenu
"" Highlights 80th column
if exists('+colorcolumn')
  set colorcolumn=80
else
  au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
endif

" enable syntax and plugins (for netrw)
filetype plugin on
"filetype plugin indent on
"filetype indent on


" GoVim: ======================================================================
set backspace=2
if has("patch-8.1.1904")
  set completeopt+=popup
  set completepopup=align:menu,border:off,highlight:Pmenu
endif

" Plugin Configurations: =====================================================
try " Go configs
	" disable all linters as that is taken care of by coc.nvim
	let g:go_diagnostics_enabled = 0
	let g:go_metalinter_enabled = []
	" don't jump to errors after metalinter is invoked
	let g:go_jump_to_error = 0
	" run go imports on file save
	let g:go_fmt_command = "goimports"
	" automatically highlight variable your cursor is on
	let g:go_auto_sameids = 0

	let g:go_highlight_types = 1
	let g:go_highlight_fields = 1
	let g:go_highlight_functions = 1
	let g:go_highlight_function_calls = 1
	let g:go_highlight_operators = 1
	let g:go_highlight_extra_types = 1
	let g:go_highlight_build_constraints = 1
	let g:go_highlight_generate_tags = 1

	" Show the function signature for a given routine with \ + i:
	autocmd BufEnter *.go nmap <leader>i  <Plug>(go-info)
	" Show the interfaces a type implements with \ + ii:
	autocmd BufEnter *.go nmap <leader>ii  <Plug>(go-implements)
	" Describe the definition of a given type with \ + ci:
	autocmd BufEnter *.go nmap <leader>ci  <Plug>(go-describe)
	" See the callers of a given function with \ + cc:
	autocmd BufEnter *.go nmap <leader>cc  <Plug>(go-callers)
	" Find all references of a given type/function in the codebase with \ + cr:
	nmap <leader>cr <Plug>(coc-references)
	" Go to definition/Go back with Ctrl+d and Ctrl+a:
	nmap <C-a> <C-o>
	nmap <C-d> <Plug>(coc-definition)
	" Not many options here, but there’s renaming the symbol your cursor is on with \ + r:
	nmap <leader>r <Plug>(coc-rename)
endtry

try " Tagbar
	nmap <F8> :TagbarToggle<CR>
endtry

try
	" FZF
	nmap <SPACE><SPACE> :Files<CR>
	nmap <SPACE>. :Rg<CR>
	nmap <SPACE>, :Buff<CR>
endtry

try
	" NERDTree
	" Netrw settings
	" let g:netrw_browse_split = 1
	" let g:netrw_winsize = 25
	" let g:netrw_banner = 0
	" let g:netrw_liststyle = 3
	" let g:netrw_browse_split = 4
	" let g:netrw_altv = 1
	" let g:netrw_winsize = 25
	"augroup ProjectDrawer
	"  autocmd!
	"  autocmd VimEnter * :Vexplore
	"augroup END

	map <C-n> :NERDTreeToggle<CR>
endtry

try
	" NERD Commenter
	let g:NERDCreateDefaultMappings = 1
	let g:NERDSpaceDelims = 1
	let g:NERDCompactSexyComs = 1
	let g:NERDDefaultAlign = 'left'
	let g:NERDCommentEmptyLines = 1
	let g:NERDTrimTrailingWhitespace = 1
	let g:NERDToggleCheckAllLines = 1
endtry

try
	"" Prettier
	"" Prettier config
	let g:prettier#autoformat = 0
	"" max line length that prettier will wrap on
	"" Prettier default: 80
	let g:prettier#config#print_width = 120
	"" number of spaces per indentation level
	"" Prettier default: 2
	let g:prettier#config#tab_width = 4
	"" use tabs over spaces
	"" Prettier default: false
	"let g:prettier#config#use_tabs = ''
	"" print semicolons
	"" Prettier default: true
	let g:prettier#config#semi = 'true'
	"" single quotes over double quotes
	"" Prettier default: false
	let g:prettier#config#single_quote = 'false'
	"" print spaces between brackets
	"" Prettier default: true
	let g:prettier#config#bracket_spacing = 'true'
	"" put > on the last line instead of new line
	"" Prettier default: false
	let g:prettier#config#jsx_bracket_same_line = 'false'
	"" avoid|always
	"" Prettier default: avoid
	let g:prettier#config#arrow_parens = 'always'
	"" none|es5|all
	"" Prettier default: none
	let g:prettier#config#trailing_comma = 'all'
	"" flow|babylon|typescript|css|less|scss|json|graphql|markdown
	"" prettier default: babylon
	let g:prettier#config#parser = ''
	"" cli-override|file-override|prefer-file
	let g:prettier#config#config_precedence = 'prefer-file'
	"" always|never|preserve
	let g:prettier#config#prose_wrap = 'preserve'
	"" css|strict|ignore
	let g:prettier#config#html_whitespace_sensitivity = 'css'
	"" autocmd BufWritePre,InsertLeave *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml,*.html PrettierAsync
endtry

try
	" Rust conf
	let g:rustfmt_autosave = 1
	let g:rust_clip_command = 'pbcopy'
endtry

" THEME: ======================================================================
try
	"Credit joshdick
	"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
	"If you're using tmux version 2.2 or later, you can remove the
	"outermost $TMUX check and use tmux's 24-bit color support
	"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more
	"information.)
	if (empty($TMUX))
		if (has("nvim"))
			"For Neovim 0.1.3 and 0.1.4
			"< https://github.com/neovim/neovim/pull/2198 >
			let $NVIM_TUI_ENABLE_TRUE_COLOR=1
		endif
		"For Neovim > 0.1.5 and Vim > patch 7.4.1799
		"< https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
		"Based on Vim patch 7.4.1770 (`guicolors` option)
		"< https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
		" < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
		if (has("termguicolors"))
			set termguicolors
		endif
	else
		set termguicolors
	endif

	" set t_Co=256   " This is may or may not needed.
	" set background=light
	set background=dark
	colorscheme PaperColor
	" colorscheme nord
endtry

try
	" Airline configuration
	set laststatus=2

	" let g:airline_theme='nord'
	let g:airline_theme='papercolor'
	let g:airline_powerline_fonts = 1
	"set t_Co=256
	let g:airline#extensions#tabline#enabled = 1
	" let g:airline#extensions#tabline#left_sep = ' '
	" let g:airline#extensions#tabline#left_alt_sep = '|'
	let g:airline#extensions#tabline#formatter = 'default'

	" let g:lightline = { 'colorscheme': 'PaperColor' }
endtry

" PaperColor Theme configuration
try
	let g:PaperColor_Theme_Options = {
	\   'theme': {
	\     'default': {
	\       'transparent_background': 1
	\     }
	\   }
	\ }
endtry
